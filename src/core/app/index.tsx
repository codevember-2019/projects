import React, { useEffect } from 'react'

import { Main } from '@core/three'
import { getDayFileWithConfKey } from '@core/utils'

import './App.scss'

const CustomComponent = getDayFileWithConfKey('component')

export default () => {
  const title = (separtor = '|') =>
    `Day ${process.env.DAY} ${separtor} ${process.env.TITLE}`

  useEffect(() => {
    document.title = title()

    const main = async () => {
      const app = new Main()
      await app.init()
      await app.run()
    }
    main()
  }, [])

  return (
    <div className="main">
      <div className="three-container">
        <div id="three" />
        {CustomComponent && <CustomComponent />}
      </div>

      <div className="footer">{title('/')}</div>
    </div>
  )
}
