import * as THREE from 'three'

import { getDayConfProperty } from '@core/utils'

class Scene {
  // HTMLElements
  private containerElement: HTMLElement = document.getElementById('three')

  // scene
  private width: number
  private height: number
  private aspectRatio: number

  // render
  private renderer: THREE.WebGLRenderer
  private scene: THREE.Scene

  // camera
  private fov: number
  private camera: THREE.Camera

  // getter
  getScene() {
    return this.scene
  }

  init() {
    return new Promise(async resolve => {
      this.initScene()
      this.initCamera()
      this.initLights()
      this.bindEvents()

      resolve()
    })
  }

  animate() {
    this.renderer.setAnimationLoop(this.render.bind(this))
  }

  private initScene() {
    this.width = this.containerElement.offsetWidth
    this.height = this.containerElement.offsetHeight

    this.aspectRatio = this.width / this.height

    // renderer
    this.renderer = new THREE.WebGLRenderer({
      antialias: true
    })

    this.scene = new THREE.Scene()

    this.renderer.setSize(this.width, this.height)
    this.containerElement.appendChild(this.renderer.domElement)
  }

  private render() {
    this.renderer.render(this.scene, this.camera)
  }

  private initCamera() {
    this.fov = getDayConfProperty('camera.fov', 45)
    const pos: THREE.Vector3 = new THREE.Vector3(
      ...getDayConfProperty('camera.position', [10, 10, 10])
    )

    this.camera = new THREE.PerspectiveCamera(
      this.fov,
      this.aspectRatio,
      1,
      2000
    )
    this.camera.position.set(pos.x, pos.y, pos.z)
    this.camera.lookAt(0, 0, 0)

    this.scene.add(this.camera)
  }

  private initLights() {}

  private bindEvents() {
    window.addEventListener('resize', () => {
      this.width = this.containerElement.offsetWidth
      this.height = this.containerElement.offsetHeight

      this.aspectRatio = this.width / this.height
      this.renderer.setSize(this.width, this.height)
    })
  }
}

export default Scene
