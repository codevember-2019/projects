import { Scene } from '@core/three'

import { getDayFileWithConfKey } from '@core/utils'

const MainScript = getDayFileWithConfKey('script', null)

class Main {
  private scene: Scene

  private main: any

  constructor() {
    this.scene = new Scene()
  }

  async init(): Promise<any> {
    await this.scene.init()
    this.scene.animate()

    this.main = new MainScript(this.scene.getScene())
    if (this.main.init) this.main.init()
  }

  run() {
    if (this.main.run) this.main.run()
  }
}

export default Main
