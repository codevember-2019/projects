import get from 'lodash.get'

const getDayConf = () => {
  try {
    return require(`@days/${process.env.DAY}/day.conf.json`)
  } catch {
    throw new Error(
      `Le fichier \'day.conf.json\' n\'existe pas pour le jour ${process.env.DAY}`
    )
  }
}

const getDayFile = (filePath: string) => {
  try {
    return require(`@days/${process.env.DAY}/${filePath}`).default
  } catch {
    throw new Error(
      `Le fichier ${filePath} n\'existe pas pour le jour ${process.env.DAY}`
    )
  }
}

const getDayFileWithConfKey = (key: string, defaultValue: any = null) => {
  try {
    const conf = getDayConf()
    const filePath = get(conf, key)

    return require(`@days/${process.env.DAY}/${filePath}`).default
  } catch {
    return defaultValue
  }
}

const getDayConfProperty = (key: string, defaultValue: any = null) => {
  try {
    const conf = getDayConf()
    return get(conf, key, defaultValue)
  } catch {
    return defaultValue
  }
}

export { getDayConf, getDayFile, getDayFileWithConfKey, getDayConfProperty }
