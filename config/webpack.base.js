const path = require('path')

const webpack = require('webpack')
const webpackMode = require('webpack-mode')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Dotenv = require('dotenv-webpack')
const notifier = require('node-notifier')

const PATHS = require('./paths')
const ALIAS = require('./alias')

module.exports = {
  entry: {
    bundle: [
      '@babel/polyfill',
      path.join(PATHS.CORE, 'index.tsx'),
      path.join(PATHS.STYLES, 'index.scss')
    ]
  },
  resolve: {
    modules: ['node_modules', PATHS.CORE],
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    alias: {
      ...ALIAS,
      three$: 'three/build/three.min.js',
      'three/.*$': 'three'
    }
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        loader: 'babel-loader'
      },
      {
        test: /\.tsx?$/,
        enforce: 'pre',
        exclude: '/node_modules/',
        loader: 'tslint-loader'
      },
      {
        test: /\.(sc|sa|c)ss$/,
        exclude: '/node_modules/',
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [
                require('autoprefixer')(),
                require('cssnano')()
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: webpackMode.isDevelopment
            }
          },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [path.join(PATHS.STYLES, 'variables', '_variables.scss'), ]
            },
          },
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(PATHS.PUBLIC, 'index.html'),
      filename: 'index.html',
      inject: 'body',
      hash: true
    }),
    new MiniCssExtractPlugin({
      filename: webpackMode.isDevelopment ? '[name].css' : '[name].[hash].css',
      chunkFilename: webpackMode.isDevelopment ? '[id].css' : '[id].[hash].css'
    }),
    new webpack.ProvidePlugin({
      THREE: 'three'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(
          webpackMode.isDevelopment ? 'development' : 'production'
        )
      }
    }),
    new Dotenv(),
    new FriendlyErrorsWebpackPlugin({
      compilationSuccessInfo: {
        messages: ['http://localhost:8080']
      },
      onErrors: (severity, errors) => {
        if (severity !== 'error') return

        const error = errors[0]
        notifier.notify({
          title: 'Error',
          message: `${severity} : ${error.name}`,
          subtitle: error.file || '',
          icon: path.join(PATHS.IMAGES, 'crying-face.png'),
          timeout: 3
        })
      }
    })
  ]
}
