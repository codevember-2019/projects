const path = require('path')

module.exports = {
  ROOT: path.resolve(__dirname, '../'),
  CONFIG: path.resolve(__dirname, '../config'),
  DIST: path.resolve(__dirname, '../', 'dist'),
  PUBLIC: path.resolve(__dirname, '../', 'public'),
  SRC: path.resolve(__dirname, '../', 'src'),
  CORE: path.resolve(__dirname, '../', 'src', 'core'),
  DAYS: path.resolve(__dirname, '../', 'src', 'days'),
  IMAGES: path.resolve(__dirname, '../', 'public', 'assets', 'images'),
  STYLES: path.resolve(__dirname, '../', 'public', 'styles')
}
