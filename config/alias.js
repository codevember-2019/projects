const path = require('path')

const PATHS = require('./paths')

module.exports = {
  '@config': path.resolve(PATHS.CONFIG),
  '@core': path.resolve(PATHS.CORE),
  '@days': path.resolve(PATHS.DAYS)
}
