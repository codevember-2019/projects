const merge = require('webpack-merge')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const baseConfig = require('./webpack.base')
const PATHS = require('./paths')

module.exports = merge(baseConfig, {
  mode: 'production',
  output: {
    path: PATHS.DIST,
    filename: '[name].[chunkhash].js',
    chunkFilename: '[id].bundle.js'
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          compress: {
            pure_funcs: ['console.log']
          }
        }
      })
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      verbose: true
    }),
    new CompressionWebpackPlugin({
      test: /\.js$/i,
      minRatio: 2,
      filename: '[path].jsgz[query]'
    })
  ]
})
