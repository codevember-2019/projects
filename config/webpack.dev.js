const merge = require('webpack-merge')

const baseConfig = require('./webpack.base')

module.exports = merge(baseConfig, {
  mode: 'development',
  output: {
    publicPath: '/'
  },
  devServer: {
    port: 8080,
    historyApiFallback: true,
    quiet: true
  },
  devtool: 'cheap-eval-source-map'
})
