#!/bin/bash

## UTILS
read_var() {
  VAR=$(grep $1 .env | xargs)
  IFS="=" read -ra VAR <<< "$VAR"
  echo ${VAR[1]}
}

DAY=$(read_var DAY)
USER=$(read_var USER)
SERVER="$(read_var SERVER)"
FOLDER=$(read_var FOLDER)


CURRENT_DAY_FOLDER="days/day-$DAY"
SERVER_PATH="$USER@$SERVER"
FROM="./dist/"
TO="$SERVER_PATH:$FOLDER/$CURRENT_DAY_FOLDER/"

echo "$FROM $TO"

ssh "$SERVER_PATH" "mkdir -p $FOLDER/$CURRENT_DAY_FOLDER"

rsync -atvze ssh "$FROM" "$TO"
